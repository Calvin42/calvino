import os
import sys
import json
import pprint
import random

#return a data file with json content and filenode
def openJson(nome, mode):
	fileName = nome+".json"
	jsonData = open(fileName,  mode)
	res = {"json":json.load(jsonData), "file": jsonData}
	return res

def closeJson(fileNode):
	fileNode.close()
#################################################
#	STATS 										#
#################################################
def checkForStat(person):
	data = openJson("data/stats", "r+")
	# print("CHECK")
	# pprint.pprint (data)
	for el in data["json"]:
		pprint.pprint(el)
		if el["person"] == person:
			closeJson(data["file"])
			return True
	closeJson(data["file"])
	return False

def addStat( person):
	data = openJson("data/stats", "a+")
	data["json"].update({"person": person, "count": 1})
	json.dump(data["json"], data["file"], indent = 4)
	closeJson(data["file"])

def updateStats( person):
	data = openJson("data/stats", "r+")
	# print("UPDATE")
	# pprint.pprint(data)
	for el in data["json"]:
		if el["person"] == person:
			el["count"] += 1
	data["file"].seek(0)
	json.dump(data["json"], data["file"], indent=4)
	closeJson(data["file"])


#################################################
#	QUOTES 										#
#################################################

def addQuote(person, quote):
	data = openJson("data/quotes", "r")
	closeJson(data["file"])
	data["json"].append({"person":person, "quote": quote})
	with open("data/quotes.json", "w+") as f:
		json.dump(data["json"], f, indent = 4)
	# closeJson(data["file"])
	return "Aggiunta!"

def getQuoteOf(person):
	data = openJson("data/quotes", "r")
	tmp = []
	for el in data["json"]:
		if el["person"] == person:
			tmp.append(el["quote"])
	closeJson(data["file"])
	if len(tmp)==0:
		return "Non ho trovato nulla di " + person +" :("
	number = random.randint(0, len(tmp)-1)
	return tmp[number]

def getRandomQuote():
	data = openJson("data/quotes", "r")
	tmp = []
	for el in data["json"]:
		tmp.append(el["quote"])
	closeJson(data["file"])
	if len(tmp)==0:
		return "Non so quotare ancora :("
	number = random.randint(0, len(tmp)-1)
	return tmp[number]
