#!/usr/bin/env python3
#-*- coding: utf-8 -*-
import telepot
import telepot.async
from telepot.delegate import per_chat_id, per_from_id
from telepot.async.delegate import create_open
import asyncio
import traceback
import jsonHandler
import datetime
import time
import random
import sys
import pprint
import timer
from concurrent.futures import ThreadPoolExecutor

#'àèéìòù'
# \xe0\xe8\xe9\xec\xf2\xf9'

####################################
#	Defining constants			   #
####################################
GROUP = "group"
PRIVATE = "private"
actualtime = time.localtime()
botCurtime = {
        "hr": actualtime.tm_hour,
        "mn": actualtime.tm_min,
        "sc": actualtime.tm_sec
        }



def getHourMinSecFromSec(sec):
    tmp = datetime.timedelta(seconds=sec)
    m, s = divmod(tmp.seconds, 60)
    h, m = divmod(m, 60)
    return {
            "hr":h+1,
            "mn":m,
            "sc": s
            }

    def checkHourMinSec(hr, mn, sc):
        if hr < 0 or hr > 24:
            return False
        elif mn < 0 or mn > 59:
            return False
        elif sc < 0 or sc > 59:
            return False
        return True

# tra 10 secondi
# 10 secondi
#    |
#   |

def getTime(timeUnit, when):
    if timeUnit in when:
    # print (str(timeUnit) + " nella frase")
        endIndex = when.index(timeUnit)
        if when[endIndex-1] == " ": #whitespace
            endIndex -=2
        if when[endIndex-1] == " ": #one digit
            startIndex = endIndex
        else: #two digits
            startIndex = endIndex - 1
        return when[startIndex:endIndex+1]
    return 0

def getSecondsFrom(hour, mins, sec):
    seconds = sec
    seconds += mins*60
    seconds += hour*60*60
    return seconds


class HandleMessage(telepot.helper.ChatHandler): #extending the chatHandler class of helper in telepot

    def __init__(self, seed_tuple, timeout):
        super(HandleMessage, self).__init__(seed_tuple, timeout)
        self._default = "@CalvinoBot"
        self._commands = """
        Ecco la lista di cosa posso fare:\n
        /roll20 - tiro un dado da 20 (o altri numeri)\n
        /stats - mostro le stats\n
        /quote - scelgo una quote casuale\n
        /quote di - scelgo una quote inserita da qualcuno\n
        /addquote - aggiungo una quote alla lista\n
        /presentati - mi presento\n
        /ricordami di - cerco di ricordarti qualcosa, puoi chiedermi:\n"/ricordami di leggere il libro di Fabio Volo tra 1 ora"\n
        poi magari non te lo ricordo perch\xe9 Fabio Volo fa schifo, ma \xe8 un altro discorso\n
        /help - vi elencher\xf2 questa lista di comandi
        """
        self._maruPhrases = ["ahahahah!", "Buongiorno!",
                u"Ciao ragazzi! Ho saltato tutto e ora vi racconto una cosa random che mi \xe8 successa oggi!",
                "Su con la vita!", u"Rega ho conosciuto una troppo carina mi sono innamorato",
                u"Ho conosciuto una ragazza troppo carina! No non \xe8 la stessa dell'altra volta"]
        #end of __init__

        def _presentYourself(self):
            phrase = "Salve, sono Calvino, relazioni umane-cyborg\nIl mio codice si trova qui: https://bitbucket.org/Calvin42/calvino"
            return phrase

        def _getStats(self):
            tmp = jsonHandler.openJson("data/stats","r")
            tmp["file"].close()
            ris = ""
            for el in tmp["json"]:
                ris+= el["person"] + " => " + str(el["count"]) + "\n"
                return ris

            def _handleStats(self, person):
                if jsonHandler.checkForStat(person):
                    jsonHandler.updateStats(person)
                else:
                    jsonHandler.addStat(person)

            def _getRandomQuote(self):
                return jsonHandler.getRandomQuote()
            def _getRandomQuoteOf(self, person):
                return jsonHandler.getQuoteOf(person)
            def _addQuote(self,person, quote):
                if len(quote) > 0:
                    return jsonHandler.addQuote(person, quote)
                else:
                    return "Consiglio di aggiungere del testo da inserire come citazione"

            def _randomMaru(self):
                rand = random.randint(0, len(self._maruPhrases)-1)
                return self._maruPhrases[rand]

            def _showHelp(self):
                return self._commands

            def _rollADice(self, num):
                return random.randint(1,num)

                # def _remind(self, what, when):


        @asyncio.coroutine
        def on_message(self, msg):

            msgTime = getHourMinSecFromSec(msg["date"])
            contentType, chatType, chatId = telepot.glance2(msg)
            msgFrom = msg["from"]["username"]
            msgText = msg["text"]
            # checking if msg is old, i.e. the bot wasn't active
            if int(msgTime["hr"]) <= int(botCurtime["hr"]) and int(msgTime["mn"]) <= int(botCurtime["mn"]) and int(msgTime["sc"]) < int(botCurtime["sc"]):
                print ("Old message")
                print ("=========================")
                if chatType == GROUP:	#if that's a group chat then I have to store the old information
                    self._handleStats(msgFrom)
                print ("=========================")
                return
            if chatType == GROUP:
                self._handleStats(msgFrom)

            if self._default in msgText:
                msgText = msgText.replace(self._default, "").strip()
            # printing msg information, just to have it on terminal
            print ("========GLANCE===========")
            print (telepot.glance2(msg))
            print ("=======MESSAGE===========")
            pprint.pprint(msg)
            print ("=========================")
            if msgText.startswith("/"): #query
                if msgText.startswith("/roll"): # roll a dice
                    number = msgText.replace("/roll", "").strip() #get the number after /roll
                    # yield from self.sender.sendMessage(self._rollADice(int(number)))
                    #print ("somethign")
                    yield from self.sender.sendMessage("Pap\xe0 non vuole che giochi con i dadi :(")
                elif msgText.startswith("/stats"): # print stats
                    yield from self.sender.sendMessage(self._getStats())
                elif msgText.startswith("/presentati"): # the bot present himself
                    yield from self.sender.sendMessage(self._presentYourself())
                elif msgText.startswith("/quote"): # get a random quote
                    yield from self.sender.sendMessage(self._getRandomQuote())
                elif msgText.startswith("/quote di"): # get a random quote of a specific person
                    tmp = msgText.replace("/quote di", "").strip()
                    yield from self.sender.sendMessage(self._getRandomQuoteOf(tmp))
                elif msgText.startswith("/addquote"): #add a quote
                    quote = msgText.replace("/addquote", "").strip()
                    yield from self.sender.sendMessage(self._addQuote(msgFrom, quote))
                elif msgText.startswith("/help"):
                    yield from self.sender.sendMessage(self._showHelp())
                elif msgText.startswith("/random_maru"):
                    yield from self.sender.sendMessage(self._randomMaru())

        @asyncio.coroutine
        def on_close(self, exception):
            print('%s %d: closed' % (type(self).__name__, self.id))

#end of HandleMessage

class FaceToFaceMessage(telepot.helper.UserHandler):
    def __init__(self, seed_tuple, timeout):
        super(FaceToFaceMessage, self).__init__(seed_tuple, timeout)
                # self._count = 0

        def _remind(self, what, when):
            seconds = 0
            hr =0
            mn = 0
            sc = 0
            if "oggi alle" in when: #oggi alle 17:40
                when = when.replace("oggi alle", "").strip()
                tmp = when.split(":")
                if len(tmp) > 1:
                    hr = int(tmp[0])
                    mn = int(tmp[1])
                else:
                    return "Mi sa che hai scritto male :/"
                if checkHourMinSec(hr, mn, sc):
                    seconds = getSecondsFrom(hr, mn, sc)
                else:
                    return "Orario sbagliato! Chi ti ha insegnato a leggere l'orologio?"
            elif "tra" or "fra" in when:
                if "tra" in when:
                    when = when.replace("tra", "").strip()
                else:
                    when = when.replace("fra", "").strip()
                    hr = int(getTime("ore", when))
                    if hr == 0:
                        hr = int(getTime("ora", when))
                    print ("Ore: "+str(hr))

                    mn = int(getTime("minuti", when))
                    if mn == 0:
                        mn = int(getTime("minuto", when))
                    print ("Minuti: "+str(mn))

                    sc = int(getTime("secondi", when))
                    if sc == 0:
                        sc = int(getTime("secondo", when))
                    print ("Secondi: "+str(sc))

                    if checkHourMinSec(hr, mn, sc):
                        seconds = getSecondsFrom(hr, mn, sc)
                        print ("Check passato")
                    else:
                        ris = "Orario sbagliato! Chi ti ha insegnato a leggere l'orologio?"
            if seconds > 0:
                print ("Piu' di 0 secondi")
                timer.startTimer(what, seconds)
                print("Sticazzi")
                #TODO MANDA UN MESSAGGIO AL MAIN THREAD
                #yield from self.sender.sendMessage(what)
                #self.sender.sendMessage(what)

            else:
                #yield from self.sender.sendMessage("Che te lo ricordo a fare se lo devi fare ora?")
                print ("Che te lo ricordo a fare se lo devi fare ora?")

        @asyncio.coroutine
        def wait_for_task(self, task, what):
            yield from task
            yield from self.sender.sendMessage(what)

        @asyncio.coroutine
        def on_message(self, msg):
            msgTime = getHourMinSecFromSec(msg["date"])
            contentType, chatType, chatId = telepot.glance2(msg)
            msgFrom = msg["from"]["username"]
            msgText = msg["text"]
            ris = ""
            if msgText.startswith("/ricordami di"): #remind something at some point of the day
                remind = msgText.replace("/ricordami di", "")
                tmp = msgText.split("/ricordami di ")
                if len(tmp) > 1:
                    what = tmp[1]
                    when = tmp[1]
                else:
                    yield from self.sender.sendMessage("Mi sa che hai scritto male :/")
                    return
                task = loop.run_in_executor(executor, self._remind, what.strip(), when.strip())
                print ("cosette 1")
                asyncio.run_coroutine_threadsafe(self.wait_for_task(task, what), loop)
                print ("cosette 2")

        @asyncio.coroutine
        def on_close(self, exception):
            print('%s %d: closed' % (type(self).__name__, self.id))
            print (str(exception))
# end of FaceToFaceMessage


token = sys.argv[1] #Passing the token via argument

#bot is the bot active listening for messages
bot = telepot.async.DelegatorBot(token, [
    #per_chat_id means the bot takes messages from a specific chat
    (per_chat_id(), create_open(HandleMessage, timeout=86400)), #86400 is the number of seconds per day
    (per_from_id(), create_open(FaceToFaceMessage, timeout=86400))
    ])

#

executor = ThreadPoolExecutor(2)
#using asyncio to get a infinite loop
loop = asyncio.get_event_loop()
loop.create_task(bot.messageLoop())
print('Listening ...')

loop.run_forever()
